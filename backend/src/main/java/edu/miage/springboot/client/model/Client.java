package edu.miage.springboot.client.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import edu.miage.springboot.evenement.model.Evenement;
import edu.miage.springboot.user.Personne;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
public class Client extends Personne {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;


    @OneToMany(mappedBy = "client")
    private List<Evenement> evenementList = new ArrayList<>();

    private String entrepriseName;

    public Long getId() {
        return id;
    }

    public List<Evenement> getEvenementList() {
        return evenementList;
    }

    public Client() {}

    public void setEvenementList(List<Evenement> evenementList) {
        this.evenementList = evenementList;
    }

    public String getEntrepriseName() {
        return entrepriseName;
    }

    public void setEntrepriseName(String entrepriseName) {
        this.entrepriseName = entrepriseName;
    }

}
