package edu.miage.springboot.participant.model;

import edu.miage.springboot.inscription.model.Inscription;
import edu.miage.springboot.user.Personne;
import jakarta.persistence.*;

import java.util.List;


@Entity
public class Participant extends Personne {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;
    private String role;

    @OneToMany(mappedBy = "participant" ,fetch = FetchType.EAGER)
    private List<Inscription>  inscriptionList;

    public Participant() {}

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    public List<Inscription> getInscriptionList() {
        return inscriptionList;
    }
    public void setInscriptionList(List<Inscription> inscriptionList) {
        this.inscriptionList = inscriptionList;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
