package edu.miage.springboot.evenement.repository;

import edu.miage.springboot.evenement.model.Evenement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface EvementRepository extends JpaRepository<Evenement, Long> {
    List<Evenement> findByDateDebutAfter(Date currenDate);
    Page<Evenement> findAll(Pageable pageable);
}
