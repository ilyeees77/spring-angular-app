package edu.miage.springboot.evenement.utils;

import edu.miage.springboot.evenement.model.Evenement;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class EvenementUtils {

    private EvenementUtils(){}
    public static List<Evenement> trierEvenements(List<Evenement> evenements) {
        return evenements.stream()
                .sorted(Comparator.comparing(Evenement::getStatutPriorite)
                        .thenComparing(Evenement::getDateDebut)
                        .thenComparing(Evenement::getDateFin))
                .collect(Collectors.toList());
    }
}
