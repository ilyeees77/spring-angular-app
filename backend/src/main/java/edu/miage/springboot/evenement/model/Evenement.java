package edu.miage.springboot.evenement.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import edu.miage.springboot.client.model.Client;
import edu.miage.springboot.inscription.model.Inscription;
import edu.miage.springboot.user.entity.Utilisateur;
import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Evenement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @OneToMany(mappedBy = "evenement", fetch = FetchType.EAGER)
    private List<Inscription> inscriptionList = new ArrayList<>();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Client client;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Utilisateur user;

    @Column(name = "title")
    private String titre;
    @Column(name = "decription")
    private String description;
    @Column(name = "startDay")
    private Date dateDebut;
    @Column(name = "endDate")
    private Date dateFin;
    @Column(name = "location")
    private String lieu;
    @Column(name = "capacity")
    private int capacite;
    @Column(name = "totalPrice", nullable = false, precision = 10, scale = 2)
    private BigDecimal price;
    @Column(name = "langue", nullable = false, length = 25)
    private String langue;
    @Column(name="category")
    private String categorie;

    // Getters et Setters pour tous les champs


    public Evenement() {
    }

    public List<Inscription> getInscriptionList() {
        return inscriptionList;
    }

    /*
    public void setInscriptionList(List<Inscription> inscriptionList) {
        this.inscriptionList = inscriptionList;
    }*/

    public Long getId() {
        return id;
    }


    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public String getLieu() {
        return lieu;
    }

    public int getCapacite() {
        return capacite;
    }

    public BigDecimal getprice() {
        return price;
    }

    public String getLangue() {
        return langue;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public void setprice(BigDecimal price) {
        this.price = price;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Transient
    public String getStatus() {
        Date now = new Date();
        if (dateDebut != null && dateFin != null) {
            if (dateDebut.before(now) && dateFin.after(now)) {
                return "En cours";
            } else if (dateDebut.after(now)) {
                return "À venir";
            } else {
                return "Terminé";
            }
        }
        return null;
    }

    @Transient
    public int getStatutPriorite() {
        String statut = this.getStatus(); // Assumons que getStatus() retourne le statut comme "En cours", "À venir", "Terminé"
        switch (statut) {
            case "En cours":
                return 1;
            case "À venir":
                return 2;
            case "Terminé":
                return 3;
            default:
                return 4; // Au cas où il y a des statuts inattendus
        }
    }


}
