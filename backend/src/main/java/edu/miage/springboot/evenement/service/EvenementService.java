package edu.miage.springboot.evenement.service;

import edu.miage.springboot.evenement.model.Evenement;
import edu.miage.springboot.evenement.repository.EvementRepository;
import edu.miage.springboot.evenement.utils.EvenementUtils;
import edu.miage.springboot.inscription.model.Inscription;
import edu.miage.springboot.inscription.service.InscriptionService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class EvenementService {

    @Autowired
    private EvementRepository evementRepository;

    @Autowired
    private InscriptionService inscriptionService;

    public Page<Evenement> getAllEvents(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        List<Evenement> eventsList = evementRepository.findAll();

        List<Evenement> evenementsTries = EvenementUtils.trierEvenements(eventsList);

        return evementRepository.findAll(pageable).getSize() > 0 ? evementRepository.findAll(pageable) : new PageImpl<>(evenementsTries);
    }

    public List<Evenement> getAllEvents(){
        List<Evenement> evenements = evementRepository.findAll();
        List<Evenement> evenementsTries = EvenementUtils.trierEvenements(evenements);

        return evenementsTries;
    }

    public Evenement getDetailEvent(Long id) {
        return evementRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("EVENT NOT FOUND"));
    }


    public Evenement saveEvent(Evenement evenement) {
        return evementRepository.save(evenement);
    }

    @Transactional
    public Object deleteEvent(Long id) {

        Map<String, Object> response = new HashMap<>();

        Evenement evenement = evementRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("EVENT NOT FOUND"));

        List<Inscription> inscriptions = evenement.getInscriptionList();

        inscriptionService.deleteInscriptionByEvenementId(id);
        evementRepository.deleteById(id);

        response.put("event_name" , evenement.getTitre());
        response.put("event_id", evenement.getId());
        response.put("deleted_inscription(s)", inscriptions.stream().map(Inscription::toString));

        return response;

    }
    public List<Evenement> getEventDateDebutAfter(Date currenDay){
        return evementRepository.findByDateDebutAfter(currenDay);
    }
}
