package edu.miage.springboot.evenement.controller;


import edu.miage.springboot.evenement.model.Evenement;
import edu.miage.springboot.evenement.service.EvenementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
@RestController
@RequestMapping("api/evenements")
@CrossOrigin(origins = "http://localhost:4200/dashboardUsers")
public class EvenementController {

    @Autowired
    private EvenementService evenementService;

    @CrossOrigin(origins = "*")
    @GetMapping
    public Object getAllEvenements(@RequestParam(defaultValue = "false") Boolean pagination,
                                            @RequestParam(defaultValue = "0") int page,
                                            @RequestParam(defaultValue = "10") int size
                                            ) {
       return pagination ? evenementService.getAllEvents(page, size) : evenementService.getAllEvents();
    }

    @GetMapping("/{id}")
    public Evenement getEventDetails(@PathVariable Long id ) {
        return evenementService.getDetailEvent(id);
    }


    @PostMapping
    public ResponseEntity<Evenement> createEvenement(@RequestBody Evenement evenement) {
        Evenement savedEvent = evenementService.saveEvent(evenement);

        return ResponseEntity.status(HttpStatus.CREATED).body(savedEvent);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEvenement(@PathVariable Long id){
        try{
            Object response = evenementService.deleteEvent(id);
            return ResponseEntity.ok(response);
        }catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error lors de la suppression");
        }

    }
    @GetMapping("/startingAfter")
    public List<Evenement> getEventsStartingAfter(@RequestParam("currentDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date currentDay){
        return evenementService.getEventDateDebutAfter(currentDay);
    }

}
