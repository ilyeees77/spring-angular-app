package edu.miage.springboot.inscription.model;


import com.fasterxml.jackson.annotation.*;
import edu.miage.springboot.evenement.model.Evenement;
import edu.miage.springboot.participant.model.Participant;
import jakarta.persistence.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Inscription {

    public Inscription(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime dateInscription;

    @ManyToOne
    @JoinColumn(name = "participant_id")
    private Participant participant;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "evenement_id")
    private Evenement evenement;

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Evenement getEvenement() {
        return evenement;
    }

    public void setEvenement(Evenement evenement) {
        this.evenement = evenement;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @PrePersist
    protected void onPrePersist(){
        this.dateInscription = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "{id :" + id + '}';
    }

    public JSONObject toJson() throws JSONException {

        JSONObject json = new JSONObject();
        json.put("id_inscription", this.id);

        return json;
    }
}
