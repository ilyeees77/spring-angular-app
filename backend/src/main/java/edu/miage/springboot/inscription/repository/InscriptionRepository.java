package edu.miage.springboot.inscription.repository;

import edu.miage.springboot.inscription.model.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InscriptionRepository extends JpaRepository<Inscription, Long> {
    public void deleteByEvenementId(Long id);

    public void deleteByParticipantId(Long id);
}
