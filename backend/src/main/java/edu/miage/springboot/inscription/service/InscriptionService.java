package edu.miage.springboot.inscription.service;


import edu.miage.springboot.inscription.repository.InscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InscriptionService {

    private InscriptionRepository inscriptionRepository;

    @Autowired
    public InscriptionService(InscriptionRepository inscriptionRepository){
        this.inscriptionRepository = inscriptionRepository;
    }

    public void deleteInscriptionByEvenementId(Long id){
        inscriptionRepository.deleteByEvenementId(id);
    }

    public void deleteInscriptionsByParticipantId(Long participantId) {
        inscriptionRepository.deleteByParticipantId(participantId);
    }

}
