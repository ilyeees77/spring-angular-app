package edu.miage.springboot.user.repository;

import edu.miage.springboot.user.entity.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>{
    Optional<Utilisateur> findByVerificationCode(String code);
    Utilisateur findByEmail(String email);
}
