package edu.miage.springboot.user.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import edu.miage.springboot.evenement.model.Evenement;
import edu.miage.springboot.user.Personne;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;



@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
public class Utilisateur extends Personne {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<Evenement> evenementList = new ArrayList<>();
    @Column(length = 250, nullable = false)
    private String password;
    @Column(length = 250, nullable = true)
    private Boolean enabled;

    @Column(length = 250, nullable = true)
    private String verificationCode;

    public Utilisateur() {}

    public Long getId() {
        return id;
    }
    public String getPassword() {return password;}
    public boolean getEnabled() {return enabled;}

    public void setId(Long id) {
        this.id = id;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setEnabled(boolean enabled) {this.enabled = enabled;}

    public List<Evenement> getEvenementList() {
        return evenementList;
    }

    public void setEvenementList(List<Evenement> evenementList) {
        this.evenementList = evenementList;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

}
