package edu.miage.springboot.user;

import jakarta.persistence.*;

@MappedSuperclass
abstract public class Personne {

    @Column(nullable = false, length = 250)
    private String firstName;
    @Column(nullable = false, length = 250)
    private String lastName;

    @Column(unique = true, length = 250, nullable = false)
    private String email;

    @Column(unique = true, length = 250, nullable = true)
    private String phoneNumber;

    public Personne() {
    }

    public String getFistName(){
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) { this.email = email; }

    public String getFirstName() {
        return firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
