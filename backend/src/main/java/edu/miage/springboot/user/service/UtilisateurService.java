package edu.miage.springboot.user.service;

import edu.miage.springboot.user.entity.Utilisateur;
import edu.miage.springboot.user.repository.UtilisateurRepository;
import jdk.jshell.execution.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.rmi.server.UID;
import java.util.UUID;


@Service
public class UtilisateurService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UtilisateurService() {}

    public Boolean verifyCode(String email, String code) {
        Utilisateur utilisateur = utilisateurRepository.findByEmail(email);
        if(utilisateur != null && utilisateur.getVerificationCode().equals(code)) {
            utilisateur.setEnabled(true);
            utilisateur.setVerificationCode(null);
            utilisateurRepository.save(utilisateur);
            return true;
        }
        return false;
    }

    public Utilisateur registerUtilisateur(Utilisateur utilisateur){

        utilisateur.setPassword(passwordEncoder.encode(utilisateur.getPassword()));
        utilisateur.setEnabled(false);

        String verificationCode = UUID.randomUUID().toString();
        utilisateur.setVerificationCode(verificationCode);

        Utilisateur savedUser = utilisateurRepository.save(utilisateur);

        return savedUser;
    }

}
