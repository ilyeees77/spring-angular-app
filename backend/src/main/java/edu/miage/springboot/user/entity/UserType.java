package edu.miage.springboot.user.entity;

public enum UserType {
    CLIENT, PARTICIPANT_INTERNE, PARTICIPANT_EXTERNE
}
