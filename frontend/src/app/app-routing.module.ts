import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserScreenComponent } from './user-page/user-screen/user-screen.component';
import { HOMEPAGEComponent } from './home-page/home-page.component';

const routes: Routes = [
  {path:'', component: HOMEPAGEComponent },
  {path: 'dashboardUsers', component: UserScreenComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
