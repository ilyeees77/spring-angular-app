import {Component, OnInit} from '@angular/core';

import { faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { EvenementService, EvenementType } from '../../shared/evenement/evenement.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})

export class HomeComponent implements OnInit{

  faBars = faBars;
  faSearch = faSearch;
  faTrash = faTrash;
  faEdit = faEdit;


  evenements: EvenementType[] = [];

  evenementKeys: string[] = [
    'titre',
    'lieu',
    'categorie',
    'date Debut',
    'date Fin',
    'capacite',
    'statut',
    'actions'
  ];
  
  showSearchInput : boolean = false;

  constructor( private evenementService : EvenementService) {}

  
  refreshEvenenemnts() : void {
    this.evenementService.getAllEvenements().subscribe((evenements) => {
      this.evenements = evenements.map((evenement) => {
        return {
          ...evenement,
          dateDebut:  new Date(evenement.dateDebut).toLocaleDateString("fr-FR", { year: 'numeric', month: 'short', day: '2-digit' }),
          dateFin:  new Date(evenement.dateFin).toLocaleDateString("fr-FR", { year: 'numeric', month: 'short', day: '2-digit' }),
        };
      });
    }
  )}

  deleteEvenement(id: number): void {
    this.evenementService.deleteEvenement(id).subscribe(() => {
      this.refreshEvenenemnts();
    }, (error) => {
      console.log(error);
    });
  }

  ngOnInit(): void {

    this.evenementService.getAllEvenements().subscribe((evenements) => {
      this.evenements = evenements.map((evenement) => {
        return {
          ...evenement,
          dateDebut:  new Date(evenement.dateDebut).toLocaleDateString("fr-FR", { year: 'numeric', month: 'short', day: '2-digit' }),
          dateFin:  new Date(evenement.dateFin).toLocaleDateString("fr-FR", { year: 'numeric', month: 'short', day: '2-digit' }),
        };
      });
    });
  }
}