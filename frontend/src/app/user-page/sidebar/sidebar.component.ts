import { Component } from '@angular/core';
import { faHomeAlt } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faAdd } from '@fortawesome/free-solid-svg-icons';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { faDeleteLeft } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons'; 
import { faUsers } from '@fortawesome/free-solid-svg-icons';


import { StateService } from '../../shared/state/state.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss'
})
export class SidebarComponent {

  faHomeAlt = faHomeAlt;
  faSearch = faSearch;
  faAdd = faAdd;
  faCog = faCog;
  faUserEdit = faUserEdit;
  faDeleteLeft=faDeleteLeft;
  faEdit=faEdit;
  faSignOutAlt= faSignOutAlt;
  faUserPlus=faUserPlus;
  faUsers=faUsers;

  constructor(private stateService: StateService, private router: Router) {}

  toggleSearchInput() {
    this.stateService.toggleSearchInput();
  }

  onShowModal() {
    this.stateService.shaowModal();
  }

  onLogoutClick() : void {
    this.router.navigate(['/']);
  }

}
