import { Component, OnInit } from '@angular/core';
import { StateService } from '../../shared/state/state.service';
import { trigger, transition, style, animate } from '@angular/animations';
import { AnimationEvent } from '@angular/animations';
import { faClose } from '@fortawesome/free-solid-svg-icons';


import { faHomeAlt } from '@fortawesome/free-solid-svg-icons';
// icon pour retourner en arriere svp import moi ça 
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'], // Corrigé ici
  animations: [
    trigger('modalAnimation', [
      transition(':enter', [
        style({ opacity: 0, transform: 'scale(0.5)' }),
        animate('0.5s ease-out', style({ opacity: 1, transform: 'scale(1)' })),
      ]),
      transition(':leave', [
        animate('0.5s ease-in', style({ opacity: 0, transform: 'scale(0.3)' }))
      ])
    ])
  ]
})
export class ModalComponent implements OnInit {
  faClose = faArrowLeft;
  showModal: boolean = false;

  constructor(private stateService: StateService) {}

  startExitAnimation: boolean = false; // Ajoutez cette ligne


  closeModalWithAnimation() {
    // Déclenchez l'animation de sortie
    this.startExitAnimation = true; // supposez que `startExitAnimation` est votre variable de contrôle pour l'animation
  
    // Utilisez un délai ou un callback d'animation pour mettre à jour `showModal`
    setTimeout(() => {
      this.showModal = false;
      this.startExitAnimation = false; // Réinitialisez pour la prochaine ouverture
    }, 480); // Assurez-vous que ce délai correspond à la durée de votre animation de sortie
  }

  animationDone(event: AnimationEvent) {
    if (event.toState === 'void') {
      this.closeModalWithAnimation();
    }
  }

  ngOnInit() {
    this.stateService.showModal$.subscribe(show => this.showModal = show);
  }
}
