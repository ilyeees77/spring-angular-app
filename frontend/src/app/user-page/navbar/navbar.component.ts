import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { faMoon } from '@fortawesome/free-solid-svg-icons';
import { faLightbulb } from '@fortawesome/free-solid-svg-icons';


import { Component , OnInit} from '@angular/core';

import { ThemeService } from '../../shared/theme/theme.service';
import { StateService } from '../../shared/state/state.service';



@Component({
  selector: 'app-navbar',
  standalone: false,
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent implements OnInit{

  faMoon= faMoon;
  faUserAlt = faUserAlt;

  showSeacrhInput : boolean = false;

  showModal : boolean = false;
  
  constructor(private themeService : ThemeService, private stateService: StateService ){}

  toogleTheme() : void {
    this.themeService.setTheme(this.themeService.getTheme() === 'dark' ? 'light' : 'dark');
  }

  ngOnInit() : void {
    this.stateService.showSearchInput$.subscribe(
      show => this.showSeacrhInput = show
    )

  }

}
