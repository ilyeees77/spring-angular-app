import { NgModule } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { UserScreenComponent } from './user-screen.component';
import { AppRoutingModule } from '../../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommonModule } from '@angular/common';
import { ModalComponent } from '../modal/modal.component';
import { InputComponent } from '../../shared/component/input/input.component';
import { SharedComponentModule } from '../../shared/shared-component/shared-component.module';



@NgModule({
  declarations: [
    UserScreenComponent,
    SidebarComponent,
    NavbarComponent,
    HomeComponent,
    ModalComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    SharedComponentModule
  ],
  exports: [UserScreenComponent]
})
export class UserScreenModule { }
