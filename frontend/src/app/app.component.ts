import {Component} from '@angular/core';
import {faUser} from '@fortawesome/free-solid-svg-icons';
import {faUserPlus} from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {

  faUser = faUser;
  faUserPlus = faUserPlus;
  
  constructor() {
  }

}
