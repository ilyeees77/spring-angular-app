import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrl: './home-page.component.scss'
})
export class HOMEPAGEComponent {

  faSignInAlt =faUserAlt  ;

  constructor(private router :Router) {}

  onLoginClick() : void {
    this.router.navigate(['/dashboardUsers']);    
  }
}
