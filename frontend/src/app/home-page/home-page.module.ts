import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from '../shared/component/input/input.component';
import { HOMEPAGEComponent } from './home-page.component';
import { AppRoutingModule } from '../app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedComponentModule } from '../shared/shared-component/shared-component.module';

@NgModule({
  declarations: [
    HOMEPAGEComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FontAwesomeModule,
    AppRoutingModule,
    SharedComponentModule
  ],
  exports: [HOMEPAGEComponent]
})
export class HomePageModule { }
