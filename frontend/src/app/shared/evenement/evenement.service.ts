import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

export interface EvenementType {
  id: number;
  titre: string;
  description: string;
  dateDebut: string;
  dateFin: string;  
  lieu: string;
  capacite: number;
  price: number;
  langue: string;
  categorie: string;
  status: string;
}

@Injectable({
  providedIn: 'root'
})
export class EvenementService {

  private apiEvents = 'http://localhost:8080/api/evenements?pagination=true&page=0&size=10';

  constructor(private http: HttpClient) { }

  getAllEvenements(): Observable<EvenementType[]> {
    return this.http.get<any>(this.apiEvents)
    .pipe(
      map((response: any) => response?.content as EvenementType[])
    );
  }

  deleteEvenement(id: number) : Observable<void> {
    return this.http.delete<void>(`${this.apiEvents}/${id}`);
  }
}
