import { Component, Input } from '@angular/core';

export interface InputComponent {
  labelText?: string;
  inputType?: string;
  labelInside?: boolean;
  palceHolderText?: string;
}

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrl: './input.component.scss'
})
export class InputComponent {

  @Input() labelText?: string;
  @Input() inputType?: string;
  @Input() labelInside?: boolean = false;
  @Input() placeHolderText?: string;
  @Input() additionalClasses: string = '';

}
