import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private showSearchInput = new BehaviorSubject<boolean>(false);

  private showModal = new BehaviorSubject<boolean>(true);

  showSearchInput$ = this.showSearchInput.asObservable();

  showModal$ = this.showModal.asObservable();
  
  constructor() {}
  
  toggleSearchInput() {
    this.showSearchInput.next(!this.showSearchInput.value)
  }

  shaowModal () {
    this.showModal.next(true)
  }

  closeModal() {
    this.showModal.next(false)
  }

}
