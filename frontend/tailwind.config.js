/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'custom-background': "url('/assets/images/background.webp')",
        'custom-background-dark': "url('/assets/images/background2.webp')",
        'custom-background-home': "url('/assets/images/background-home.webp')",
        'custom-background-auth': "url('/assets/images/background-auth.jpeg')",
      }
    },
  },
  plugins: [],
}

